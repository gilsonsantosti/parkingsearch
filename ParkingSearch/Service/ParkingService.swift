//
//  ParkingService.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//


import Foundation
import Alamofire

class ParkingService {
    private let URLPARKINGLIST = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&radius=%@&type=parking&key=AIzaSyCTOghsZJbZaDa9R_MfxIMg_BhD0CvTSI0"
    private let URLBYPLACEID = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCTOghsZJbZaDa9R_MfxIMg_BhD0CvTSI0&placeid=%@"
    
    func getParkingList(lat:String, long:String, radius:String, completionHandler: @escaping (ResultSwift<ParkingList,ErrorType>) -> Void) {
        let urlGet = String(format: self.URLPARKINGLIST, lat, long, radius)
        
        Alamofire.request(urlGet).validate().responseJSON { (response) in
            switch response.result{
            case .success:
                if let data = response.data{
                    do{
                        let parkingList = try JSONDecoder().decode(ParkingList.self, from: data)
                        completionHandler(.success(parkingList))
                    }catch{
                        completionHandler(.failure(.generic))
                    }
                }
            case .failure:
                completionHandler(.failure(.generic))
            }
        }
    }
    
    func getParkingByPlaceId(placeId:String, completionHandler: @escaping (ResultSwift<Parking,ErrorType>) -> Void) {
        let urlGet = String(format: self.URLBYPLACEID, placeId)

        Alamofire.request(urlGet).validate().responseJSON { (response) in
            switch response.result{
            case .success:
                if let data = response.data{
                    do{
                        let parking = try JSONDecoder().decode(Parking.self, from: data)
                        completionHandler(.success(parking))
                    }catch{
                        completionHandler(.failure(.generic))
                    }
                }
            case .failure:
                completionHandler(.failure(.generic))
            }
        }
    }
}
