//
//  ResultSwift.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation
//typealias criado devido a um problema de migração do Alamofire, pois ele possui um objeto de nome Result, conflitando com o Result da linguagem na sua versao 5.0
internal typealias ResultSwift = Result
