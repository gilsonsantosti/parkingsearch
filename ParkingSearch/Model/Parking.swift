//
//  Parking.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation

class ParkingElement: Codable {
    var htmlAttributions : [String]?
    var nextPageToken : String?
    var status : String?
    enum CodingKeys: String, CodingKey {
        case htmlAttributions = "html_attributions"
        case nextPageToken = "next_page_token"
        case status = "status"
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        htmlAttributions = try values.decodeIfPresent([String].self, forKey: .htmlAttributions)
        nextPageToken = try values.decodeIfPresent(String.self, forKey: .nextPageToken)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
}

class ParkingList : ParkingElement {
    var results : [ResultElement]?
    enum CodingKeysParkingList: String, CodingKey {
        case results = "results"
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeysParkingList.self)
        results = try values.decodeIfPresent([ResultElement].self, forKey: .results)
    }
}

class Parking : ParkingElement {
    var result : ResultDetail?
    enum CodingKeysParking: String, CodingKey {
        case result = "result"
    }
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeysParking.self)
        result = try values.decodeIfPresent(ResultDetail.self, forKey: .result)
    }
}

class ResultElement : Codable {
    var geometry : Geometry?
    var icon : String?
    var id : String?
    var name : String?
    var photos : [Photos]?
    var placeId : String?
    var plusCode : PlusCode?
    var rating : Double?
    var reference : String?
    var scope : String?
    var types : [String]?
    var userRatingsTotal : Int?
    var vicinity : String?
    
    enum CodingKeysResultElement: String, CodingKey {
        case geometry = "geometry"
        case icon = "icon"
        case id = "id"
        case name = "name"
        case photos = "photos"
        case placeId = "place_id"
        case plusCode = "plus_code"
        case rating = "rating"
        case reference = "reference"
        case scope = "scope"
        case types = "types"
        case userRatingsTotal = "user_ratings_total"
        case vicinity = "vicinity"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeysResultElement.self)
        geometry = try values.decodeIfPresent(Geometry.self, forKey: .geometry)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        photos = try values.decodeIfPresent([Photos].self, forKey: .photos)
        placeId = try values.decodeIfPresent(String.self, forKey: .placeId)
        plusCode = try values.decodeIfPresent(PlusCode.self, forKey: .plusCode)
        rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        reference = try values.decodeIfPresent(String.self, forKey: .reference)
        scope = try values.decodeIfPresent(String.self, forKey: .scope)
        types = try values.decodeIfPresent([String].self, forKey: .types)
        userRatingsTotal = try values.decodeIfPresent(Int.self, forKey: .userRatingsTotal)
        vicinity = try values.decodeIfPresent(String.self, forKey: .vicinity)
    }
}

class ResultDetail : ResultElement {
    var addressComponents : [AddressComponents]?
    var adrDddress : String?
    var formattedAddress : String?
    var reviews : [Reviews]?
    var url : String?
    var utcOffset : Int?
    
    enum CodingKeysResult: String, CodingKey {
        case addressComponents = "address_components"
        case adrDddress = "adr_address"
        case formattedAddress = "formatted_address"
        case reviews = "reviews"
        case url = "url"
        case utcOffset = "utc_offset"
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeysResult.self)
        addressComponents = try values.decodeIfPresent([AddressComponents].self, forKey: .addressComponents)
        adrDddress = try values.decodeIfPresent(String.self, forKey: .adrDddress)
        formattedAddress = try values.decodeIfPresent(String.self, forKey: .formattedAddress)
        reviews = try values.decodeIfPresent([Reviews].self, forKey: .reviews)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        utcOffset = try values.decodeIfPresent(Int.self, forKey: .utcOffset)
    }
}

class AddressComponents : Codable {
    var longName : String?
    var shortName : String?
    var types : [String]?
    enum CodingKeys: String, CodingKey {
        case longName = "long_name"
        case shortName = "short_name"
        case types = "types"
    }
}

class Reviews : Codable {
    var authorName : String?
    var authorUrl : String?
    var language : String?
    var profilePhotoUrl : String?
    var rating : Int?
    var relativeTimeDescription : String?
    var text : String?
    var time : Int?
    
    enum CodingKeys: String, CodingKey {
        case authorName = "author_name"
        case authorUrl = "author_url"
        case language = "language"
        case profilePhotoUrl = "profile_photo_url"
        case rating = "rating"
        case relativeTimeDescription = "relative_time_description"
        case text = "text"
        case time = "time"
    }
}

class Geometry : Codable {
    var location : Location?
    var viewport : Viewport?
}

class Location : Codable {
    var lat : Double?
    var lng : Double?
}

class Viewport : Codable {
    var northeast : Location?
    var southwest : Location?
}

class Photos : Codable {
    var height : Int?
    var htmlAttributions : [String]?
    var photoReference : String?
    var width : Int?
    enum CodingKeys: String, CodingKey {
        case height = "height"
        case htmlAttributions = "html_attributions"
        case photoReference = "photo_reference"
        case width = "width"
    }
}

class PlusCode : Codable {
    var compound_code : String?
    var globalCode : String?
    enum CodingKeys: String, CodingKey {
        case compound_code = "compound_code"
        case globalCode = "global_code"
    }
}


