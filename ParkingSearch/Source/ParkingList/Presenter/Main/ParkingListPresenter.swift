//
//  ParkingListPresenter.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol ParkingListViewDelegate: NSObjectProtocol {
    func showLoading()
    func hideLoading()
    func showError()
    func setViewData(viewData: ParkingViewData)
}

struct ParkingViewData {
    var initialLocation = LocationViewData()
    var parkingElements = [ParkingElementViewData]()
}

struct ParkingElementViewData {
    var name = ""
    var address = ""
    var rating = ""
    var userRatingsTotal = ""
    var urlImage = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%@&key=AIzaSyCTOghsZJbZaDa9R_MfxIMg_BhD0CvTSI0"
    var placeid = ""
    var isDefaultImage = false
    var distance:Double = 0.0
    var location = LocationViewData()
}

struct LocationViewData {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
}

class ParkingListPresenter {
    private weak var viewDelegate: ParkingListViewDelegate?
    private lazy var viewData = ParkingViewData()
    private let service: ParkingService
    
    init(viewDelegate: ParkingListViewDelegate) {
        self.viewDelegate = viewDelegate
        self.service = ParkingService()
    }
}

//MARK: - METHODS PUBLICS -
extension ParkingListPresenter {
    func getParkingList(lat: String, long: String, radius: String) {
        if !Reachability.isConnectedToNetwork() {
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.showError()
            return
        }
        if let latValue = Double(lat), let longValue = Double(long) {
            self.viewData.initialLocation.latitude = latValue
            self.viewData.initialLocation.longitude = longValue
        }
        self.viewDelegate?.showLoading()
        self.service.getParkingList(lat: lat, long: long, radius: radius) { (result) in
            switch result {
            case .success(let parkingModel):
                self.parseModelFromViewData(model: parkingModel)
                if self.viewData.parkingElements.count > 0 {
                    self.viewDelegate?.setViewData(viewData: self.viewData)
                }else {
                    self.viewDelegate?.showError()
                }
                break
            case .failure(_):
                self.viewDelegate?.showError()
                break
            }
            self.viewDelegate?.hideLoading()
        }
    }
}

//MARK: - METHODS AUX -
extension ParkingListPresenter {
    
    private func parseModelFromViewData(model: ParkingList) {
        self.viewData.parkingElements.removeAll()
        if let resultModel = model.results, resultModel.count > 0 {
                resultModel.forEach({self.parseParkingElementModelFromViewData(element: $0)})
        }else {
            self.viewDelegate?.showError()
        }
        self.viewData.parkingElements = self.viewData.parkingElements.sorted(by: {$0.distance < $1.distance})
    }
    
    private func parseParkingElementModelFromViewData(element: ResultElement) {
        guard let placeId = element.placeId, let lat = element.geometry?.location?.lat, let lng = element.geometry?.location?.lng else { return }
        var viewData = ParkingElementViewData()
        viewData.name = element.name ?? "Sem nome cadastrado"
        viewData.address = element.vicinity ?? "Sem endereço"
        viewData.rating = String(format: "%.1f", element.rating ?? 0.0)
        viewData.userRatingsTotal = "(\(element.userRatingsTotal ?? 0))"
        if let imageUrl = element.photos?.first?.photoReference, !imageUrl.isEmpty {
            viewData.urlImage = String(format: viewData.urlImage, imageUrl)
        }else {
            viewData.urlImage = "parkingDefault"
            viewData.isDefaultImage = true
        }
        viewData.location.latitude = lat
        viewData.location.longitude = lng
        viewData.placeid = placeId
        viewData.distance = CLLocation(latitude: lat, longitude: lng).distance(from: CLLocation(latitude: self.viewData.initialLocation.latitude, longitude: self.viewData.initialLocation.longitude))
        self.viewData.parkingElements.append(viewData)
    }
}
