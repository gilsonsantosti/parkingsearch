//
//  DetailParkingPresenter.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 11/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation

protocol DetailParkingViewDelegate: NSObjectProtocol {
    func showLoading()
    func hideLoading()
    func showError()
    func setViewData(viewData: DetailParkingViewData)
}

struct DetailParkingViewData {
    var id = ""
    var name = ""
    var ratingTotal: Double = 0
    var userRatingsTotal = ""
    var yourRating: Double = 0
    var locale = LocationViewData()
    var userOpinion = [UserOpinion]()
}

struct UserOpinion {
    var nameUser = ""
    var description = ""
    var urlProfileImage = ""
    var rating: Double = 0
    var isDefaultImage = false
}

class DetailParkingPresenter {
    
    private weak var viewDelegate: DetailParkingViewDelegate?
    private lazy var viewData = DetailParkingViewData()
    private let service: ParkingService
    private let dataBase: RatingDataBase
    
    init(viewDelegate: DetailParkingViewDelegate) {
        self.viewDelegate = viewDelegate
        self.service = ParkingService()
        self.dataBase = RatingDataBase()
    }
}

//MARK: - METHODS PUBLICS -
extension DetailParkingPresenter {
    func getParkingByPlaceId(placeId: String) {
        if !Reachability.isConnectedToNetwork() {
            self.viewDelegate?.showError()
            return
        }
        self.viewDelegate?.showLoading()
        self.service.getParkingByPlaceId(placeId: placeId) { (result) in
            switch result {
            case .success(let parkingModel):
                self.parseModelFromViewData(model: parkingModel)
                DispatchQueue.main.async {
                    self.viewDelegate?.setViewData(viewData: self.viewData)
                }
                break
            case .failure(_):
                self.viewDelegate?.showError()
                break
            }
            self.viewDelegate?.hideLoading()
        }
    }
    
    func ratingParking(id: String, ratingNumber: Double) {
        if !id.isEmpty {
            self.dataBase.createOrUpdateRatingDataBase(id: id, ratingNumber: ratingNumber)
        }
    }
}

//MARK: - METHODS AUX -
extension DetailParkingPresenter {
    private func parseModelFromViewData(model: Parking) {
        self.viewData.userOpinion.removeAll()
        guard let lat = model.result?.geometry?.location?.lat, let lng = model.result?.geometry?.location?.lng else { return }
        self.viewData.ratingTotal = model.result?.rating ?? 0
        self.viewData.userRatingsTotal = "(\(model.result?.userRatingsTotal ?? 0))"
        if let id = model.result?.id {
            self.viewData.id = "\(id)"
            self.viewData.yourRating = self.dataBase.fetchRatingDataBase(id: id) ?? 0
        }
        self.viewData.name = model.result?.name ?? ""
        self.viewData.locale.latitude = lat
        self.viewData.locale.longitude = lng
        if let reviews = model.result?.reviews, reviews.count > 0 {
            reviews.forEach({self.parseUserOpinionModelFromViewData(review: $0)})
        }
    }
    
    private func parseUserOpinionModelFromViewData(review: Reviews) {
        var opinionViewData = UserOpinion()
        opinionViewData.nameUser = review.authorName ?? ""
        opinionViewData.description = (review.text != nil && !review.text!.isEmpty) ? review.text! : "Esse usuário não escreveu uma avaliação."
        if let imageUrl = review.profilePhotoUrl, !imageUrl.isEmpty {
            opinionViewData.urlProfileImage = imageUrl
        }else {
            opinionViewData.urlProfileImage = "profileDefault"
            opinionViewData.isDefaultImage = true
        }
        opinionViewData.rating = Double(review.rating ?? 0)
        self.viewData.userOpinion.append(opinionViewData)
    }
}
