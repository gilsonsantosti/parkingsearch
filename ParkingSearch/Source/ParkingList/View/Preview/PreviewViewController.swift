//
//  PreviewViewController.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 11/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class PreviewViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var labelName: UILabel!
    var viewData: ParkingElementViewData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
}

//MARK: - Mehtods Aux -
extension PreviewViewController {
    private func setupView() {
        if let name = self.viewData?.name, let address = self.viewData?.address {
            self.labelName.text = "\(name) localizado em \(address)"
        }
        self.setLocationInMap()
    }
    
    private func setLocationInMap() {
        guard let viewData = self.viewData else { return }
        self.mapView.mapType = .satellite
        let location = CLLocationCoordinate2D(latitude: viewData.location.latitude, longitude: viewData.location.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 0)
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        self.mapView.addAnnotation(annotation)
    }
}
