//
//  ParkingTableViewCell.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class ParkingTableViewCell: UITableViewCell {
    @IBOutlet weak var imagePark: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var viewStar: CosmosView!
    @IBOutlet weak var labelAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imagePark.kf.cancelDownloadTask()
        self.imagePark.image = nil
    }
}

//MARK: - Methods Aux -
extension ParkingTableViewCell {
    func setupCell(viewData: ParkingElementViewData) {
        if viewData.isDefaultImage {
            self.imagePark.image = UIImage(named: viewData.urlImage)
        }else {
            self.downloadImage(urlString: viewData.urlImage)
        }
        self.labelName.text = viewData.name
        self.labelRating.text = viewData.rating
        self.labelAddress.text = viewData.address
        if let ratingValue = Double(viewData.rating) {
            self.viewStar.rating = ratingValue
        }else {
            self.viewStar.rating = 0
        }
        self.viewStar.text = viewData.userRatingsTotal
    }
    
    private func downloadImage(urlString: String) {
        if let url:URL = URL(string: urlString){
            let resource = ImageResource(downloadURL: url, cacheKey: urlString)
            let processor = DownsamplingImageProcessor(size: self.imagePark.bounds.size)
                >> RoundCornerImageProcessor(cornerRadius: 20)
            self.imagePark.kf.indicatorType = .activity
            
            self.imagePark.kf.setImage(with: resource, placeholder: nil, options: [.transition(.fade(0.8)), .cacheOriginalImage, .processor(processor)], progressBlock: nil) { (result) in
                switch result {
                case .success(let imageResult):
                    self.imagePark.image = imageResult.image
                    break
                case .failure(_):
                     self.imagePark.image = UIImage(named: "parkingDefault")
                    break
                }
            }
        }
    }
}
