//
//  ViewController.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 07/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import CoreLocation

class ParkingListViewController: UIViewController {
    
    @IBOutlet weak var parkingTableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var buttonReload: UIButton!
    
    private let location = CLLocationManager()
    private var presenter: ParkingListPresenter!
    private lazy var viewData = ParkingViewData()
    private let DETAILCONTROLLERID = "DetailParking"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.location.delegate = self
        self.presenter = ParkingListPresenter(viewDelegate: self)
        location.requestWhenInUseAuthorization()
        self.registerCell()
        self.register3DTouch()
        self.addRefreshControl()
    }
    
    @IBAction func reaload(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }) { (_) in
            self.refreshService()
            sender.transform = .identity
        }
    }
}

//MARK: - UITableViewDataSource -
extension ParkingListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewData.parkingElements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.parkingTableView.dequeueReusableCell(withIdentifier: "ParkingTableViewCell") as! ParkingTableViewCell
        cell.setupCell(viewData: self.viewData.parkingElements[indexPath.row])
        return cell
    }
}

//MARK: - UITableViewDelegate -
extension ParkingListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.parkingTableView.dequeueReusableCell(withIdentifier: "MapTableViewCell") as! MapTableViewCell
        cell.setupCell(viewData: self.viewData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeId = self.viewData.parkingElements[indexPath.row].placeid
        self.showDetail(placeId: placeId)
    }
}

//MARK: - CLLocationManagerDelegate -
extension ParkingListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.location.requestLocation()
        }else if status == .denied{
            self.showError()
            self.labelError.text = "Você não autorizou o uso de sua localização e para continuar usando o App vá em configurações e autorize o uso de sua localização."
            self.buttonReload.isHidden = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lat = "\(locations.first!.coordinate.latitude)"
        let long = "\(locations.first!.coordinate.longitude)"
        self.presenter.getParkingList(lat: lat, long: long, radius: "15000")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

//MARK: - ParkingListViewDelegate -
extension ParkingListViewController: ParkingListViewDelegate {
    func showLoading() {
        UIView.animate(withDuration: 0.3) {
            self.viewSuccess.isHidden = false
            self.loading.isHidden = false
            self.loading.alpha = 1
            self.viewSuccess.alpha = 1
        }
    }
    
    func hideLoading() {
        UIView.animate(withDuration: 0.3) {
            self.loading.alpha = 0
            self.loading.isHidden = true
        }
    }
    
    func showError() {
        self.labelError.text = "Desculpe mas ocorreu um erro interno, tente novamente."
        self.viewError.alpha = 0
        self.viewError.isHidden = false
        self.loading.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.viewSuccess.alpha = 0
            self.viewError.alpha = 1
        }) { (_) in
            self.viewSuccess.isHidden = true
        }
    }
    
    func setViewData(viewData: ParkingViewData) {
        self.viewData = viewData
        self.parkingTableView.reloadData()
    }
}

//MARK: - UIViewControllerPreviewingDelegate -
extension ParkingListViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if let indexPath = self.parkingTableView.indexPathForRow(at: location), let previewController = self.storyboard?.instantiateViewController(withIdentifier: "preview") as? PreviewViewController {
            previewController.viewData = self.viewData.parkingElements[indexPath.row]
            return previewController
        }
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        guard let previewController = viewControllerToCommit as? PreviewViewController, let placeid = previewController.viewData?.placeid else {
            return
        }
        self.showDetail(placeId: placeid)
    }
}

//MARK: - Methods Aux -
extension ParkingListViewController {
    private func registerCell() {
        self.parkingTableView.register(UINib(nibName: "ParkingTableViewCell", bundle: nil), forCellReuseIdentifier: "ParkingTableViewCell")
        self.parkingTableView.register(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapTableViewCell")
    }
    
    private func register3DTouch() {
        if self.traitCollection.forceTouchCapability == .available {
            self.registerForPreviewing(with: self, sourceView: self.parkingTableView)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailController = segue.destination as? DetailParkingViewController, let placeId = sender as? String {
            detailController.placeId = placeId
        }
    }
    
    private func showDetail(placeId: String) {
        if let detailController = self.storyboard?.instantiateViewController(withIdentifier: self.DETAILCONTROLLERID) as? DetailParkingViewController {
            detailController.placeId = placeId
            detailController.userLocation = self.viewData.initialLocation
            self.present(detailController, animated: true, completion: nil)
        }
    }
    
    private func addRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshService), for: .allEvents)
        self.parkingTableView.refreshControl = refreshControl
    }
    
    @objc private func refreshService() {
        self.viewData.parkingElements.removeAll()
        self.parkingTableView.reloadData()
        self.showLoading()
        self.location.requestLocation()
        self.parkingTableView.refreshControl?.endRefreshing()
    }
}

