//
//  ReviewsHeaderTableViewCell.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 11/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit

class ReviewsHeaderTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
