//
//  DetailParkingViewController.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 11/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Cosmos

class DetailParkingViewController: UIViewController {
    
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewStarRating: CosmosView!
    @IBOutlet weak var labelStarRating: UILabel!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var viewStarYourRating: CosmosView!
    @IBOutlet weak var labelYourRating: UILabel!
    
    
    private var presenter: DetailParkingPresenter!
    private lazy var viewData = DetailParkingViewData()
    var placeId = ""
    var userLocation = LocationViewData()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.registerCell()
        self.presenter = DetailParkingPresenter(viewDelegate: self)
        self.presenter.getParkingByPlaceId(placeId: self.placeId)
        self.viewStarYourRating.didTouchCosmos = didToucheStar
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - DetailParkingViewDelegate -
extension DetailParkingViewController: DetailParkingViewDelegate {
    func setViewData(viewData: DetailParkingViewData) {
        self.viewData = viewData
        self.setupView()
        self.detailTableView.reloadData()
    }
    
    func showLoading() {
        self.viewError.isHidden = true
        self.loading.isHidden = false
        self.viewLoading.isHidden = false
    }
    
    func hideLoading() {
        UIView.animate(withDuration: 0.3, animations: {
            self.viewLoading.alpha = 0
        }) { (_) in
            self.viewLoading.isHidden = true
            self.viewLoading.alpha = 1
        }
    }
    
    func showError() {
        self.viewError.alpha = 0
        self.viewError.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.loading.alpha = 0
            self.viewError.alpha = 1
        }) { (_) in
            self.loading.isHidden = true
        }
    }
}

//MARK: - UITableViewDataSource -
extension DetailParkingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewData.userOpinion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.detailTableView.dequeueReusableCell(withIdentifier: "ReviewsTableViewCell") as! ReviewsTableViewCell
        cell.setupCell(viewData: self.viewData.userOpinion[indexPath.row])
        return cell
    }
}

//MARK: - UITableViewDelegate -
extension DetailParkingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.detailTableView.dequeueReusableCell(withIdentifier: "ReviewsHeaderTableViewCell") as! ReviewsHeaderTableViewCell
        return cell
    }
}

//MARK: - MKMapViewDelegate -
extension DetailParkingViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        polylineRenderer.lineWidth = 4
        return polylineRenderer
    }
}

//MARK: - Methods Aux -
extension DetailParkingViewController {
    private func registerCell() {
        self.detailTableView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTableViewCell")
        self.detailTableView.register(UINib(nibName: "ReviewsHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsHeaderTableViewCell")
    }
    
    private func setupView() {
        self.viewStarRating.rating = self.viewData.ratingTotal
        self.viewStarRating.text = self.viewData.userRatingsTotal
        self.labelStarRating.text = "\(self.viewData.ratingTotal)"
        self.viewStarYourRating.rating = self.viewData.yourRating
        self.labelYourRating.text = "\(self.viewData.yourRating)"
        self.showRouteInMap()
    }
    
    private func showRouteInMap() {
        let request = MKDirections.Request()
        let locationUser = CLLocationCoordinate2D(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        self.addAnotation(title: "Sua Localização", location: locationUser)
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: locationUser))
        
        let locatinDestination = CLLocationCoordinate2D(latitude: self.viewData.locale.latitude, longitude: self.viewData.locale.longitude)
        self.addAnotation(title: self.viewData.name, location: locatinDestination)
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: locatinDestination))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { [unowned self] (response, error) in
            guard let responseResult = response else { return }
            responseResult.routes.forEach({ (route) in
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            })
        }
    }
    
    private func addAnotation(title: String, location: CLLocationCoordinate2D) {
        let annotationUser = MKPointAnnotation()
        annotationUser.coordinate = location
        annotationUser.title = title
        self.mapView.addAnnotation(annotationUser)
    }
    
    private func didToucheStar(value: Double) {
        self.labelYourRating.text = "\(value)"
        self.presenter.ratingParking(id: self.viewData.id, ratingNumber: value)
    }
}
