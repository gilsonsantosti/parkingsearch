//
//  ReviewsTableViewCell.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 11/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import Kingfisher

class ReviewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelRatingNumber: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageProfile.kf.cancelDownloadTask()
        self.imageProfile.image = nil
    }
}

//MARK: - Methods Aux -
extension ReviewsTableViewCell {
    func setupCell(viewData: UserOpinion) {
        self.labelName.text = viewData.nameUser
        if viewData.isDefaultImage {
            self.imageProfile.image = UIImage(named: viewData.urlProfileImage)
        }else {
            self.downloadImage(urlString: viewData.urlProfileImage)
        }
        self.labelRating.text = viewData.description
        self.labelRatingNumber.text = "Avaliação: \(viewData.rating)"
    }
    
    private func downloadImage(urlString: String) {
        if let url:URL = URL(string: urlString){
            let resource = ImageResource(downloadURL: url, cacheKey: urlString)
            let processor = DownsamplingImageProcessor(size: self.imageProfile.bounds.size)
                >> RoundCornerImageProcessor(cornerRadius: 30)
            self.imageProfile.kf.indicatorType = .activity
            
            self.imageProfile.kf.setImage(with: resource, placeholder: nil, options: [.transition(.fade(0.8)), .cacheOriginalImage, .processor(processor)], progressBlock: nil) { (result) in
                switch result {
                case .success(let imageResult):
                    self.imageProfile.image = imageResult.image
                    break
                case .failure(_):
                    self.imageProfile.image = UIImage(named: "profileDefault")
                    break
                }
            }
        }
    }
}
