//
//  MapTableViewCell.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//MARK: - Methods Aux -
extension MapTableViewCell {
    func setupCell(viewData: ParkingViewData) {
        viewData.parkingElements.forEach({self.setLocationInMap(lat: $0.location.latitude, long: $0.location.longitude, title: $0.name)})
        self.setLocationInMap(lat: viewData.initialLocation.latitude, long: viewData.initialLocation.longitude, title: "Sua Localização")
    }
    
    private func setLocationInMap(lat:Double, long:Double, title:String) {
        let location = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = title
        self.mapView.addAnnotation(annotation)
        
    }
}
