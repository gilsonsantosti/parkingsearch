//
//  RatingDataBase.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 25/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation
import CoreData

class RatingDataBase {
    private var fetchRequest:NSFetchRequest<Rating> = Rating.fetchRequest()
}

extension RatingDataBase {
    func createOrUpdateRatingDataBase(id: String, ratingNumber: Double){
        if let ratingResult = self.getRatingById(id: id){
            self.parseFromDataBase(id: id, ratingNumber: ratingNumber, ratingDB: ratingResult)
        }else{
            let ratingCreate = Rating(context: PersistentManager.shared.context)
            self.parseFromDataBase(id: id, ratingNumber: ratingNumber, ratingDB: ratingCreate)
        }
    }
    
    func fetchRatingDataBase(id: String) -> Double?{
        if let ratingDataBase = self.getRatingById(id: id){
            return ratingDataBase.ratingNumber
        }
        return nil
    }
}

extension RatingDataBase {
    private func getRatingById(id: String) -> Rating?{
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        do {
            let result = try PersistentManager.shared.context.fetch(self.fetchRequest)
            if let favoriteDb = result.first{
                return favoriteDb
            }
        } catch {
            return nil
        }
        return nil
    }
}

extension RatingDataBase {
    private func parseFromDataBase(id: String, ratingNumber: Double, ratingDB: Rating) {
        ratingDB.id = id
        ratingDB.ratingNumber = ratingNumber
        PersistentManager.shared.saveContext()
    }
}
