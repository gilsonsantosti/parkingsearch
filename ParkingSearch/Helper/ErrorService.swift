//
//  ErrorService.swift
//  ParkingSearch
//
//  Created by Gilson Santos on 08/05/19.
//  Copyright © 2019 Gilson Santos. All rights reserved.
//

import Foundation

enum ErrorType:Error {
    case notNetwork
    case notFound
    case generic
}
